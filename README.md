# Error-correcting code  <!-- omit in toc -->

- [Description](#description)
- [Prerequisites](#prerequisites)
- [Installation & Usage](#installation--usage)
  - [Installation](#installation)
  - [Compilation](#compilation)
  - [Removing compilation artifacts](#removing-compilation-artifacts)
  - [Usage](#usage)
- [Auteur](#auteur)


## Description  

This project was made during my third year of study at Strasbourg University.
This project consists of transferring files using UNIX sockets in datagram mode
through an unreliable medium and detect/correct errors that occured during
the transfer.

[Project Gitlab](https://git.unistra.fr/tzerbib/error-detection-and-correction)


## Prerequisites

- [gcc](https://gcc.gnu.org/releases.html): No release are provided so you will
  need to compile the source yourself.
- [GNU make](https://ftp.gnu.org/gnu/make/)
- [diff](https://ftp.gnu.org/gnu/diffutils/): This tool is used to compare
  the send file and the received one. (Note that it is included in most
  LINUX kernels. You can test is it is installed by using
  the command ``diff -v``)


## Installation & Usage  

### Installation  

First you will need to download the project.
You can either clone this repository
```bash
git clone && cd error-detection-and-correction-master
```

or download a zip file
```bash
# Download the zip file
curl -LOk https://gitlab.unistra.fr/tzerbib/error-detection-and-correction/-/archive/master/error-detection-and-correction-master.zip

# Unzip the archive
unzip error-detection-and-correction-master

# Remove the zip file
rm error-detection-and-correction-master.zip && cd error-detection-and-correction-master
```

### Compilation  

You can compile the complete project using GNU make at the root directory
```bash
cd <project_root_directory>
make            # Compile complete project
```

It is also possible to compile only the sender, medium or receiver
using this command
```bash
cd <project_root_directory>
make sender     # Compile only the sender

make medium     # Compile only the medium

make receiver   # Compile only the receiver
```

The same result can be obtain by using the command into the subfolder
```bash
cd <project_root_directory/sender>
make            # Compile only the sender
```


### Removing compilation artifacts  

To remove compilation artifacts such as *.o* files, dependencies files,
and UNIX sockets used for this project, you can use the following command
```bash
make clean
```
This will remove:
- *.o* files from sender, medium or receiver
- *.d* files from sender, medium or receiver (files containing dependencies)
- UNIX socket used for this project


To get a clean repository, use
```bash
make mrproper
```
In addition to files removes by make clean, this will delete as well:
- All binary
- *.o* files from *common* path
- The file where the receiver write when using ``make test``

These commands could be launched from any of the three sudproject (sender,
medium or receiver) to remove only appropriate files...


### Usage  

To get a quick test, you can run this command directly from root directory
```bash
make test
```

This is equivalent to launch these commands (and it is what's internally done)
```bash
# Run every programs
medium/bin/medium 0 & sleep 1; receiver/bin/receiver /tmp/aaa & sleep 1; sender/bin/sender sender/src/sending.c

# Test if there is some differences between the original file and the received one
diff --color /tmp/aaa sender/src/sending.c
```

You can specify argument to pass to subprograms by overwriting MARGS, SARGS
and RARGS
```bash
# Be sure you are at the project root
cd <project_root_directory>

# This call is equivalent to `make test` (using default values)
make test MARGS=0 SARGS=src/sending.c RARGS=/tmp/aaa
```

If you currently are in a subproject and want to run only this one, you can use
a similar synthax (note that if you are in a subfolder,
*MARGS*, *SARGS* and *RARGS* are all replaced by *ARGS*
cause there isn't any ambiguity anymore)
```bash
# Assuming we are in a subproject folder
cd <project_root_directory/sender>

# This call is equivalent to `make test` (using default value)
make test ARGS=src/sending.c
```

If you have any doubt about supported make target, you can call ``make help``.
```bash
# This call
make help

# Print out this message
> Usage: make [<target>]
> 
> Available argets are:
>   all                     (default target) Compile all sender, medium and receiver.
>   sender                  Compile only the sender.
>   medium                  Compile only the medium.
>   receiver                Compile only the receiver.
>   clean                   Removes most of the files created during compilation.
>   mrproper                Get a complete clean repository.
>   dist                    Create a tar named 'TP3Network_cor.tgz' containing core files in order to distribute the project.
>   help                    Display this information message.
>   test [<options=val>]    Launch a quick test. Supported options are the following:
>                           MARGS  Options passed to medium to conduces error introduction.
>                             0: Doesn't introduce error in the message.
>                             1: Introduce a 1 bit error in each word.
>                             2: Let ERROR_RATE decide the error rate (byte granularity).
>                             3: Introduce 1 byte error in the whole message.
>                             4: Let ERROR_RATE decide the error rate (word granularity).
>                           RARGS  Options passed to receiver. (default /tmp/aaa)
>                           SARGS  Options passed to sender.   (default /src/sending.c)
>                           'make test' is equivalent to 'make test MARGS=0 SARGS=src/sending.c RARGS=/tmp/aaa'
```

## Auteur  

[ZERBIB Timothée](https://gitlab.unistra.fr/tzerbib)