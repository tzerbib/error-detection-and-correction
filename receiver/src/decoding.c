/**
 * @file decoding.c
 * @author Arash Habibi
 * @author Julien Montavont
 * @version 2.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Functions to decipher the code words
 */

#include <stdio.h>
#include "codingdecoding.h"

void copyDataBitsDecoding(CodeWord_t *cw, char *message, int data_size)
{
  int i = 0;

  for(i=0; i<data_size; i++)
  {
    setNthBitW(&(message[i]), 1, getNthBit(cw[i], 1));
    setNthBitW(&(message[i]), 2, getNthBit(cw[i], 2));
    setNthBitW(&(message[i]), 3, getNthBit(cw[i], 3));
    setNthBitW(&(message[i]), 4, getNthBit(cw[i], 4));
    setNthBitW(&(message[i]), 5, getNthBit(cw[i], 5));
    setNthBitW(&(message[i]), 6, getNthBit(cw[i], 6));
    setNthBitW(&(message[i]), 7, getNthBit(cw[i], 7));
    setNthBitW(&(message[i]), 8, getNthBit(cw[i], 8));
  }
  
}


const char H[4][8] = {{1,0,1,1,1,0,0,0},
                      {0,1,1,1,0,1,0,0},
                      {1,1,0,1,0,0,1,0},
                      {1,1,1,0,0,0,0,1}};

void errorCorrection(CodeWord_t *cw, int data_size)
{
  char w, val, res = 0, e;

  for(int i=0; i<data_size; i++)
  {
    // Print
    // printBits(cw[i], "recv");

    // Get data and control for the first part
    setNthBitW(&w, 1, getNthBit(cw[i], 1));
    setNthBitW(&w, 2, getNthBit(cw[i], 2));
    setNthBitW(&w, 3, getNthBit(cw[i], 3));
    setNthBitW(&w, 4, getNthBit(cw[i], 4));
    setNthBitW(&w, 5, getNthBit(cw[i], 9));
    setNthBitW(&w, 6, getNthBit(cw[i], 10));
    setNthBitW(&w, 7, getNthBit(cw[i], 11));
    setNthBitW(&w, 8, getNthBit(cw[i], 12));

    // Decode first part
    for(int k=0; k<4; k++){
      val = 0;
      for(int j=0; j<8; j++){
        val ^= H[k][j] & getNthBit(w, j+1);
      }
      setNthBitW(&res, k+1, val);
    }

    // Correct errors in first part
    for(int j=0; j<8; j++){
      e = 1;
      for(int k=0; k<4; k++){
        if(getNthBit(res, k+1) != H[k][j]){
          e = 0;
          break;
        }
      }
      if(e){
        // An error is located on bit j
        if(j<4)
          changeNthBitCW(&(cw[i]), j+1);
        else
          changeNthBitCW(&(cw[i]), j+5);

        res = 0;
        break;
      }
    }

    // Replace old control bits in first part
    setNthBitCW(&(cw[i]), 9, getNthBit(res, 1));
    setNthBitCW(&(cw[i]), 10, getNthBit(res, 2));
    setNthBitCW(&(cw[i]), 11, getNthBit(res, 3));
    setNthBitCW(&(cw[i]), 12, getNthBit(res, 4));


    // Get data and control for the second part
    setNthBitW(&w, 1, getNthBit(cw[i], 5));
    setNthBitW(&w, 2, getNthBit(cw[i], 6));
    setNthBitW(&w, 3, getNthBit(cw[i], 7));
    setNthBitW(&w, 4, getNthBit(cw[i], 8));
    setNthBitW(&w, 5, getNthBit(cw[i], 13));
    setNthBitW(&w, 6, getNthBit(cw[i], 14));
    setNthBitW(&w, 7, getNthBit(cw[i], 15));
    setNthBitW(&w, 8, getNthBit(cw[i], 16));

    // Decode second part
    for(int k=0; k<4; k++){
      val = 0;
      for(int j=0; j<8; j++){
        val ^= H[k][j] & getNthBit(w, j+1);
      }
      setNthBitW(&res, k+1, val);
    }


    // Correct errors in second part
    for(int j=0; j<8; j++){
      e = 1;
      for(int k=0; k<4; k++){
        if(getNthBit(res, k+1) != H[k][j]){
          e = 0;
          break;
        }
      }
      if(e){
        // An error is located on bit j
        if(j<4)
          changeNthBitCW(&(cw[i]), j+5);
        else
          changeNthBitCW(&(cw[i]), j+9);

        res = 0;
        break;
      }
    }

    // Replace old control bits in first part
    setNthBitCW(&(cw[i]), 13, getNthBit(res, 1));
    setNthBitCW(&(cw[i]), 14, getNthBit(res, 2));
    setNthBitCW(&(cw[i]), 15, getNthBit(res, 3));
    setNthBitCW(&(cw[i]), 16, getNthBit(res, 4));
  }

  return;
}


int thereIsError(CodeWord_t *cw, int data_size)
{
  int sum;

  for(int i=0; i<data_size; i++){
    // Print received message
    // printBits(cw[i], "deco");

    sum = 0;
    for(int j=0; j<3; j++)
      sum |= getNthBit(cw[i], j+9);

    if(sum)
      return 1;

    sum = 0;
    for(int j=0; j<3; j++)
      sum |= getNthBit(cw[i], j+13);

    if(sum)
      return 1;
  }

  return 0;
}


void decoding(char *cw, int cw_size, char *message, int *data_size)
{
  *data_size = cw_size / sizeof(CodeWord_t);

  errorCorrection((CodeWord_t*)cw, *data_size);

  // -- For decoding
  copyDataBitsDecoding((CodeWord_t*)cw, message, *data_size);

  // -- For error detection
  if(thereIsError((CodeWord_t*)cw, *data_size))
  {
    printf("PARITY ERROR: \"%s\"\n", message);
  }

  return;
}
