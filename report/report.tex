% Pour vérifier qu'on écrit du LaTeX moderne
\RequirePackage[orthodox] {nag}

\documentclass[oneside,openright,a4paper,11pt,french]{article}

% Règles de typographie françaises
\usepackage[francais]{babel}

% Jeu de caractères UTF-8
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% Fonte élégante
\usepackage{mathpazo}
\usepackage[scaled]{helvet}
\usepackage{courier}

% pour \EUR
\usepackage{marvosym}

% Utilisation de tableaux
\usepackage{tabularx}

% Utilisation d'url
\usepackage{hyperref}
\urlstyle{sf}

% Utilisation de listings
\usepackage{listings}
\lstset{
  language=c,
  basicstyle=\small\ttfamily,
  texcl=true,                       % latex ds commentaires C "//"
  commentstyle=\sffamily\itshape,   % commentaires en sans-serif
  keywordstyle=                     % pas de mise en valeur
}

% Utilisation d'images
\usepackage{graphicx}
\setkeys{Gin}{keepaspectratio}	% par défaut : conserver les proportions

% Puces des listes
\usepackage{enumitem}
\setitemize{label=\textbullet}

% Matrices
\usepackage{amsmath}

% Pseudo-code
\usepackage[french, onelanguage, ruled, lined]{algorithm2e}

% Pour utiliser des couleurs
\usepackage{xcolor}

% Définition des marges
\usepackage[margin=25mm, foot=15mm] {geometry}

\parskip=2mm
\parindent=0mm

\newcommand\comment[1]{\footnotesize\ttfamily\textcolor{green}{#1}}
\pagestyle{plain}

\title{L3 RLI - TP Projet \\ Codes détecteurs/correcteurs d’erreur}
\author{Timothée ZERBIB}


\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\maketitle

\section{Introduction}

L'objectif de ce projet est de modéliser et d'implémenter différentes versions
de méthodes de détection et correction d'erreurs. Seule demeure dans le code
la dernière méthode, capable de détecter jusqu'à 2 erreurs et de corriger
la transmission dans le cas où une unique erreur a eu lieu.

Ce projet est constitué d'un programme émetteur, d'un programme récepteur et
d'un programme servant de médium de transmission. Ces différents programmes
ont été séparés dans 3 répertoires distincts, disposant chacun d'un Makefile
permettant la génération d'un binaire. Un Makefile mère est situé à la racine
du projet et permet la génération des différents exécutables, ainsi que
le lancement d'un test ou encore la génération du présent rapport.
Pour plus d'informations sur son utilisation, utiliser la cible \texttt{help}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modélisation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Modélisation}

Dans un but de simplicité, les exemples de ce rapport ne tiendront pas compte
de la façon dont sont encodés et traités les entiers, et notamment leur sens.

\subsection{Version 0}

Dans cette partie, on considère $H_0$ la matrice de parité suivante\,:

\begin{center}
    $H_0 = 
    \begin{pmatrix}
      1 & 0 & 1 & 0 & 1 & 0 \\
      0 & 1 & 1 & 0 & 0 & 1
    \end{pmatrix}$
\end{center}

\subsubsection{Matrice de parité et matrice génératrice}
\label{const_H0G0}

On remarque que la matrice $H_0$ s'écrit comme\,:
\[
H_0 = 
\left(
\begin{array}{c|c}
  A^t & I_2 \\
\end{array}
\right)
\]

\newpage

On peut ainsi immédiatement déterminer la matrice génératrice $G_0$\:

\[
G_0 = 
\left(
\begin{array}{c|c}
  I_4 & A \\
\end{array}
\right)
=
\begin{pmatrix}
  1 & 0 & 0 & 0 & 1 & 0 \\
  0 & 1 & 0 & 0 & 0 & 1 \\
  0 & 0 & 1 & 0 & 1 & 1 \\
  0 & 0 & 0 & 1 & 0 & 0
\end{pmatrix}
\]

De façon plus générale,


$H_0 \cdot C^t = 0_{2,1}$

$
\Leftrightarrow
\begin{pmatrix}
  1 & 0 & 1 & 0 & 1 & 0 \\
  0 & 1 & 1 & 0 & 0 & 1
\end{pmatrix}
\cdot
\begin{pmatrix}
  c_1  \\
  c_2  \\
  c_3  \\
  c_4  \\
  c_5  \\
  c_6
\end{pmatrix}
=
\begin{pmatrix}
  c_1 + c_3 + c_5 \\
  c_2 + c_3 + c_6
\end{pmatrix}
=
\begin{pmatrix}
  0 \\
  0
\end{pmatrix}
$

$
\Leftrightarrow
\left \{
  \begin{array}{r c l}
    c_1 + c_3 + c_5  & = & 0 \\
    c_2 + c_3 + c_6  & = & 0
  \end{array}
\right .
$

\vspace{3mm}
Plusieurs choix sont alors possibles pour les bits de contrôle\,:

\begin{itemize}
  \item{
    Si on choisit les positions 5 et 6, on retombe sur la transformation
    systématique\,:
    
    $
    \left \{
      \begin{array}{r c l}
        c_5  & = & c_1 + c_3 \\
        c_6  & = & c_2 + c_3
      \end{array}
    \right .
    $
    
    \vspace{3mm}

    Or $D \cdot G_0 = C$
    
    \vspace{1mm}
    
    $
    \begin{pmatrix}
      c_1 & c_2 & c_3 & c_4
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      w_1 & w_2 & w_3 & w_4 & w_5 & w_6 \\
      x_1 & x_2 & x_3 & x_4 & x_5 & x_6 \\
      y_1 & y_2 & y_3 & y_4 & y_5 & y_6 \\
      z_1 & z_2 & z_3 & z_4 & z_5 & z_6
    \end{pmatrix}
    =
    \begin{pmatrix}
      c_1 & c_2 & c_3 & c_4 & c_1+c_3 & c_2+c_3
    \end{pmatrix}
    $

    d'où
    
    $G_0 = 
    \begin{pmatrix}
      1 & 0 & 0 & 0 & 1 & 0 \\
      0 & 1 & 0 & 0 & 0 & 1 \\
      0 & 0 & 1 & 0 & 1 & 1 \\
      0 & 0 & 0 & 1 & 0 & 0
    \end{pmatrix}$
    \vspace{5mm}
  }
  \item{
    Si on choisit les positions 1 et 2, on obtient\,:
    
    $
    \left \{
      \begin{array}{r c l}
        c_1  & = & c_3 + c_5 \\
        c_2  & = & c_3 + c_6
      \end{array}
    \right .
    $
    
    \vspace{3mm}

    Or $D' \cdot {G_0}' = C'$
    
    \vspace{1mm}
    
    $
    \begin{pmatrix}
      c_3 & c_4 & c_5 & c_6
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      w_1 & w_2 & w_3 & w_4 & w_5 & w_6 \\
      x_1 & x_2 & x_3 & x_4 & x_5 & x_6 \\
      y_1 & y_2 & y_3 & y_4 & y_5 & y_6 \\
      z_1 & z_2 & z_3 & z_4 & z_5 & z_6
    \end{pmatrix}
    =
    \begin{pmatrix}
      c_3+c_5 & c_3+c_6 & c_3 & c_4 & c_5 & c_6
    \end{pmatrix}
    $

    d'où
    
    ${G_0}' = 
    \begin{pmatrix}
      1 & 1 & 1 & 0 & 0 & 0 \\
      0 & 0 & 0 & 1 & 0 & 0 \\
      1 & 0 & 0 & 0 & 1 & 0 \\
      0 & 1 & 0 & 0 & 0 & 1
    \end{pmatrix}$
    \vspace{5mm}
  }
  \item{
    Si on choisit les positions 1 et 6, on obtient\,:
    
    $
    \left \{
      \begin{array}{r c l}
        c_1  & = & c_3 + c_5 \\
        c_6  & = & c_2 + c_3
      \end{array}
    \right .
    $
    
    \vspace{3mm}

    Or $D'' \cdot {G_0}'' = C''$
    
    \vspace{1mm}
    
    $
    \begin{pmatrix}
      c_2 & c_3 & c_4 & c_5
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      w_1 & w_2 & w_3 & w_4 & w_5 & w_6 \\
      x_1 & x_2 & x_3 & x_4 & x_5 & x_6 \\
      y_1 & y_2 & y_3 & y_4 & y_5 & y_6 \\
      z_1 & z_2 & z_3 & z_4 & z_5 & z_6
    \end{pmatrix}
    =
    \begin{pmatrix}
      c_3+c_5 & c_2 & c_3 & c_4 & c_5 & c_2+c_3
    \end{pmatrix}
    $

    d'où
    
    ${G_0}'' = 
    \begin{pmatrix}
      0 & 1 & 0 & 0 & 0 & 1 \\
      1 & 0 & 1 & 0 & 0 & 1 \\
      0 & 0 & 0 & 1 & 0 & 0 \\
      1 & 0 & 0 & 0 & 1 & 0
    \end{pmatrix}$
    \vspace{5mm}
  }
  \item{
    Si on choisit les positions 5 et 2, on obtient\,:
    
    $
    \left \{
      \begin{array}{r c l}
        c_5  & = & c_1 + c_3 \\
        c_2  & = & c_3 + c_6
      \end{array}
    \right .
    $
    
    \vspace{3mm}

    Or $D''' \cdot {G_0}''' = C'''$
    
    \vspace{1mm}
    
    $
    \begin{pmatrix}
      c_1 & c_3 & c_4 & c_6
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      w_1 & w_2 & w_3 & w_4 & w_5 & w_6 \\
      x_1 & x_2 & x_3 & x_4 & x_5 & x_6 \\
      y_1 & y_2 & y_3 & y_4 & y_5 & y_6 \\
      z_1 & z_2 & z_3 & z_4 & z_5 & z_6
    \end{pmatrix}
    =
    \begin{pmatrix}
      c_1 & c_3+c_6 & c_3 & c_4 & c_1+c_3 & c_6
    \end{pmatrix}
    $

    d'où
    
    ${G_0}''' = 
    \begin{pmatrix}
      1 & 0 & 0 & 0 & 1 & 0 \\
      0 & 1 & 1 & 0 & 1 & 0 \\
      0 & 0 & 0 & 1 & 0 & 0 \\
      0 & 1 & 0 & 0 & 0 & 1
    \end{pmatrix}$
    \vspace{5mm}
  }
\end{itemize}


\subsubsection{Exemples d'encodages et de décodages}

Essayons d'encoder puis de décoder la lettre \texttt{a} en utilisant les couples
$(H_0,G_0)$, $(H_0,{G_0}')$, $(H_0,{G_0}'')$ et $(H_0,{G_0}''')$.

La lettre \texttt{a} a pour code ascii 97 = 0b01100001, il faut donc la séparer
en deux blocs de 4 bits pour pouvoir l'encoder à l'aide de
notre matrice génératrice. Elle sera donc représentée par la matrice 
$ C =
  \big(\begin{smallmatrix}
  0 & 1 & 1 & 0 \\
  0 & 0 & 0 & 1
\end{smallmatrix}\big)$.

Dans le message à décoder, les bits de données sont représentés en bleu
et les bits de contrôle en rouge.

\begin{itemize}
  \item{
    Encodage et décodage en utilisant le couple $(H_0, G_0)$

    Encodage\,:

    $D \cdot G_0 = C$
    
    $
    \Leftrightarrow
    \begin{pmatrix}
      0 & 1 & 1 & 0 \\
      0 & 0 & 0 & 1
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      1 & 0 & 0 & 0 & 1 & 0 \\
      0 & 1 & 0 & 0 & 0 & 1 \\
      0 & 0 & 1 & 0 & 1 & 1 \\
      0 & 0 & 0 & 1 & 0 & 0
    \end{pmatrix}
    =
    \begin{pmatrix}
      \textcolor{blue}{0} & \textcolor{blue}{1} & \textcolor{blue}{1} & \textcolor{blue}{0} & \textcolor{red}{1} & \textcolor{red}{0} \\
      \textcolor{blue}{0} & \textcolor{blue}{0} & \textcolor{blue}{0} & \textcolor{blue}{1} & \textcolor{red}{0} & \textcolor{red}{0}
    \end{pmatrix}
    $

    \newpage

    Décodage\,: (On vérifie que $H_0 \cdot C^t = 0_2$)

    $
    \begin{pmatrix}
      1 & 0 & 1 & 0 & 1 & 0 \\
      0 & 1 & 1 & 0 & 0 & 1
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      \textcolor{blue}{0} & \textcolor{blue}{0}  \\
      \textcolor{blue}{1} & \textcolor{blue}{0}  \\
      \textcolor{blue}{1} & \textcolor{blue}{0}  \\
      \textcolor{blue}{0} & \textcolor{blue}{1}  \\
      \textcolor{red}{1} & \textcolor{red}{0}  \\
      \textcolor{red}{0} & \textcolor{red}{0}
    \end{pmatrix}
    =
    \begin{pmatrix}
      0 & 0  \\
      0 & 0
    \end{pmatrix}
    $
  }
  \item{
    Encodage et décodage en utilisant le couple $(H_0, {G_0}')$

    Encodage\,:

    $D \cdot {G_0}' = C'$
    
    $
    \Leftrightarrow
    \begin{pmatrix}
      0 & 1 & 1 & 0 \\
      0 & 0 & 0 & 1
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      1 & 1 & 1 & 0 & 0 & 0 \\
      0 & 0 & 0 & 1 & 0 & 0 \\
      1 & 0 & 0 & 0 & 1 & 0 \\
      0 & 1 & 0 & 0 & 0 & 1
    \end{pmatrix}
    =
    \begin{pmatrix}
      \textcolor{red}{1} & \textcolor{red}{0} & \textcolor{blue}{0} & \textcolor{blue}{1} & \textcolor{blue}{1} & \textcolor{blue}{0} \\
      \textcolor{red}{0} & \textcolor{red}{1} & \textcolor{blue}{0} & \textcolor{blue}{0} & \textcolor{blue}{0} & \textcolor{blue}{1}
    \end{pmatrix}
    $

    \vspace{3mm}
    Décodage\,: (On vérifie que $H_0 \cdot {C'}^t = 0_2$)

    $
    \begin{pmatrix}
      1 & 0 & 1 & 0 & 1 & 0 \\
      0 & 1 & 1 & 0 & 0 & 1
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      \textcolor{red}{1} & \textcolor{red}{0}  \\
      \textcolor{red}{0} & \textcolor{red}{1}  \\
      \textcolor{blue}{0} & \textcolor{blue}{0}  \\
      \textcolor{blue}{1} & \textcolor{blue}{0}  \\
      \textcolor{blue}{1} & \textcolor{blue}{0}  \\
      \textcolor{blue}{0} & \textcolor{blue}{1}
    \end{pmatrix}
    =
    \begin{pmatrix}
      0 & 0  \\
      0 & 0
    \end{pmatrix}
    $
  }
  \item{
    Encodage et décodage en utilisant le couple $(H_0, {G_0}'')$

    Encodage\,:

    $D \cdot {G_0}'' = C''$
    
    $
    \Leftrightarrow
    \begin{pmatrix}
      0 & 1 & 1 & 0 \\
      0 & 0 & 0 & 1
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      0 & 1 & 0 & 0 & 0 & 1 \\
      1 & 0 & 1 & 0 & 0 & 1 \\
      0 & 0 & 0 & 1 & 0 & 0 \\
      1 & 0 & 0 & 0 & 1 & 0
    \end{pmatrix}
    =
    \begin{pmatrix}
      \textcolor{red}{1} & \textcolor{blue}{0} & \textcolor{blue}{1} & \textcolor{blue}{1} & \textcolor{blue}{0} & \textcolor{red}{1} \\
      \textcolor{red}{1} & \textcolor{blue}{0} & \textcolor{blue}{0} & \textcolor{blue}{0} & \textcolor{blue}{1} & \textcolor{red}{0}
    \end{pmatrix}
    $

    \vspace{3mm}
    Décodage\,: (On vérifie que $H_0 \cdot {C''}^t = 0_2$)

    $
    \begin{pmatrix}
      1 & 0 & 1 & 0 & 1 & 0 \\
      0 & 1 & 1 & 0 & 0 & 1
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      \textcolor{red}{1} & \textcolor{red}{1}  \\
      \textcolor{blue}{0} & \textcolor{blue}{0}  \\
      \textcolor{blue}{1} & \textcolor{blue}{0}  \\
      \textcolor{blue}{1} & \textcolor{blue}{0}  \\
      \textcolor{blue}{0} & \textcolor{blue}{1}  \\
      \textcolor{red}{1} & \textcolor{red}{0}
    \end{pmatrix}
    =
    \begin{pmatrix}
      0 & 0  \\
      0 & 0
    \end{pmatrix}
    $
  }
  \item{
    Encodage et décodage en utilisant le couple $(H_0, {G_0}''')$

    Encodage\,:

    $D \cdot {G_0}''' = C'''$
    
    $
    \Leftrightarrow
    \begin{pmatrix}
      0 & 1 & 1 & 0 \\
      0 & 0 & 0 & 1
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      1 & 0 & 0 & 0 & 1 & 0 \\
      0 & 1 & 1 & 0 & 1 & 0 \\
      0 & 0 & 0 & 1 & 0 & 0 \\
      0 & 1 & 0 & 0 & 0 & 1
    \end{pmatrix}
    =
    \begin{pmatrix}
      \textcolor{blue}{0} & \textcolor{red}{1} & \textcolor{blue}{1} & \textcolor{blue}{1} & \textcolor{red}{1} & \textcolor{blue}{0} \\
      \textcolor{blue}{0} & \textcolor{red}{1} & \textcolor{blue}{0} & \textcolor{blue}{0} & \textcolor{red}{0} & \textcolor{blue}{1}
    \end{pmatrix}
    $

    \newpage

    Décodage\,: (On vérifie que $H_0 \cdot {C'''}^t = 0_2$)

    $
    \begin{pmatrix}
      1 & 0 & 1 & 0 & 1 & 0 \\
      0 & 1 & 1 & 0 & 0 & 1
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      \textcolor{blue}{0} & \textcolor{blue}{0}  \\
      \textcolor{red}{1}  & \textcolor{red}{1}   \\
      \textcolor{blue}{1} & \textcolor{blue}{0}  \\
      \textcolor{blue}{1} & \textcolor{blue}{0}  \\
      \textcolor{red}{1}  & \textcolor{red}{0}   \\
      \textcolor{blue}{0} & \textcolor{blue}{1}
    \end{pmatrix}
    =
    \begin{pmatrix}
      0 & 0  \\
      0 & 0
    \end{pmatrix}
    $
  }
\end{itemize}


\subsubsection{Détection et correction d'erreurs}

Étudions le décodage du mot 0000(00) lorsque celui-ci est reçu sans erreur
ou avec une unique erreur dont la position varie.

$
H_0 \cdot C^t =
\begin{pmatrix}
  1 & 0 & 1 & 0 & 1 & 0 \\
  0 & 1 & 1 & 0 & 0 & 1
\end{pmatrix}
\cdot
\begin{pmatrix}
  0 & 1 & 0 & 0 & 0 & 0 & 0 \\
  0 & 0 & 1 & 0 & 0 & 0 & 0 \\
  0 & 0 & 0 & 1 & 0 & 0 & 0 \\
  0 & 0 & 0 & 0 & 1 & 0 & 0 \\
  0 & 0 & 0 & 0 & 0 & 1 & 0 \\
  0 & 0 & 0 & 0 & 0 & 0 & 1
\end{pmatrix}
=
\begin{pmatrix}
  0 & 1 & 0 & 1 & 0 & 1 & 0 \\
  0 & 0 & 1 & 1 & 0 & 0 & 1
\end{pmatrix}
$

Lorsque le message est transmis sans erreur comme pour $C_1$,
on obtient une colonne nulle, comme attendu.
Lorsqu'une erreur survient, la colonne résultante du décodage est bien identique
à la colonne de $H_0$ qui indique la position de l'erreur dans $C^t$,
or comme la 1\up{ère} et 5\up{ème} colonne de $H_0$ sont identiques, il est
impossible de différencier une erreur sur le 1\up{er} ou le 5\up{ème}
bit d'un message. Il en est de même pour le 2\up{ème} et le 6\up{ème} bit.
Enfin, la 4\up{ème} colonne de $H_0$ étant nulle, il est impossible
de différencier une erreur sur le 4\up{ème} bit d'un message, d'un message
transmis sans erreur.
Ainsi, la 3\up{ème} colonne de $H_0$ étant la seule colonne unique, il n'est
possible de déterminer une erreur et de la corriger que si celle-ci est
située sur le 3\up{ème} bit et que le message n'en contient aucune autre.

En comparaison, en utilisant les codes de Hamming, pour $r = 2$
bits de contrôle, il est possible de transmettre au maximum
$k = n-r = (2^r-1)-r = 1$ bits d'information en garantissant une détection
et correction d'une erreur.
En utilisant 3 bits de contrôle, le nombre de bits d'information
transmissibles en conservant les mêmes garanties passe à $k = (2^3-1)-3 = 4$


\subsection{Version 1}

Pour s'assurer de la détection d'une erreur quelle que soit sa position, il faut
modifier la 4\up{ème} colonne de $H_0$ afin qu'elle soit non nulle.
Un exemple de cette nouvelle matrice de parité est\,:

$
H_1 = 
\begin{pmatrix}
  1 & 0 & 1 & \textcolor{red}{1} & 1 & 0 \\
  0 & 1 & 1 & \textcolor{red}{1} & 0 & 1
\end{pmatrix}
$

On peut alors déterminer la matrice $G_1$ en utilisant
la méthode systématique\,:

$
H_1 = 
\left(
\begin{array}{c|c}
  {A_1}^t & I_2 \\
\end{array}
\right)
$

d'où

$
G_1 = 
\left(
\begin{array}{c|c}
  I_4 & A_1 \\
\end{array}
\right)
\begin{pmatrix}
  1 & 0 & 0 & 0 & 1 & 0 \\
  0 & 1 & 0 & 0 & 0 & 1 \\
  0 & 0 & 1 & 0 & 1 & 1 \\
  0 & 0 & 0 & 1 & \textcolor{red}{1} & \textcolor{red}{1}
\end{pmatrix}
$

\newpage

Le couple $(H_1, G_1)$ offrant une distance de Hamming de 2, il permet
la détection d'une erreur mais il est toujours impossible de la corriger,
puisque plusieurs colonnes de $H_1$ sont égales.
Son rendement est
$R = \frac{\mathit{debit\_utile}}{\mathit{debit\_total}}
= \frac{4}{6} = \frac{2}{3}$.

En comparaison, un code de Hamming(3,1) offrant une distance de Hamming de 3,
permet la détection de jusqu'à 2 erreurs ou la correction d'une unique,
mais pour un rendement de seulement $\frac{1}{3}$.
Un code de Hamming(7,4), quant à lui, offre un rendement de $\frac{4}{7}$
pour les mêmes propriétés.



\subsection{Version 2}

\subsubsection{Matrice de parité et matrice génératrice}
\label{const_H2G2}

Commençons par transformer notre couple $(H_1, G_1)$ afin d'obtenir
un code de Hamming(7,4).
La nouvelle matrice aura donc $n=7$ colonnes et $n-k=3$ lignes.
Chacune de ces colonnes doit être unique et non nulle. De plus, je choisis
mes transformations afin de conserver la matrice identité en bloc du côté droit
afin de pouvoir utiliser la transformation systématique de H en G.

$
H_1 = 
\begin{pmatrix}
  1 & 0 & 1 & 1 & 1 & 0 \\
  0 & 1 & 1 & 1 & 0 & 1
\end{pmatrix}
$

$
{H_1}' = 
\begin{pmatrix}
  1 & 0 & 1 & 1 & 1 & 0 & \textcolor{red}{0} \\
  0 & 1 & 1 & 1 & 0 & 1 & \textcolor{red}{0} \\
  \textcolor{red}{1} & \textcolor{red}{1} & \textcolor{red}{0} & \textcolor{red}{1} & \textcolor{red}{0} & \textcolor{red}{0} & \textcolor{red}{1}
\end{pmatrix}
$

Cette nouvelle matrice nous permet d'obtenir une distance de Hamming de 3,
or pour pouvoir différencier 2 erreurs d'une erreur unique,
il nous faut une distance de Hamming $d \geq 4$ (Un mot contenant
une unique erreur aura une distance de 1 avec un mot du code et pourra alors
être corrigé, tandis qu'un mot ayant subi deux erreurs sera à distance 2,
soit à équidistance de deux mots du code et sera donc identifié
comme ayant subi 2 erreurs. Avec une distance de Hamming de seulement 3, un mot
ayant subi deux transformations sera bien à distance 2 de son mot de départ,
mais se retrouvera à distance 1 d'un autre mot du code).

Pour augmenter la distance entre les mots du code, il faut
ajouter un bit de contrôle.
La matrice $H_2$ possède donc $n=7+1=8$ colonnes
et $n-k = 8-4 = 4$ lignes et doit contenir la matrice $I_4$.
Chacune des colonnes de $H_2$ doit être différente afin d'être capable
de localiser une erreur. \\
De plus, la combinaison de deux colonnes quelconques de $H_2$
ne doit être égale à aucune de ses colonnes afin d'être capable de différencier
une erreur unique d'une erreur double.
Comme la matrice contient l'identité, ses autres colonnes doivent posséder
exactement trois 1. En effet, une colonne avec seulement deux 1 combinée
à une des deux colonnes de l'identité dont le 1 est placé
sur la même ligne que l'un des deux aurait pour résultat l'autre colonne de
l'identité (cf. Exemple 1)\,;
une colonne avec quatre 1 combinée à une des colonnes à trois 1 donnerait,
quant à elle, une des colonnes de l'identité (cf. Exemple 2).

Exemple 1\,:
$
\begin{pmatrix}
  1 \\
  1 \\
  0 \\
  0
\end{pmatrix}
+
\begin{pmatrix}
  1 \\
  0 \\
  0 \\
  0
\end{pmatrix}
=
\begin{pmatrix}
  0 \\
  1 \\
  0 \\
  0
\end{pmatrix}
$

Exemple 2\,:
$
\begin{pmatrix}
  1 \\
  1 \\
  1 \\
  1
\end{pmatrix}
+
\begin{pmatrix}
  1 \\
  1 \\
  1 \\
  0
\end{pmatrix}
=
\begin{pmatrix}
  0 \\
  0 \\
  0 \\
  1
\end{pmatrix}
$

\vspace{5mm}
On obtient donc la matrice de parité suivante\,:

$
H_2 = 
\begin{pmatrix}
  1 & 0 & 1 & 1 & 1 & 0 & \textcolor{red}{0} & \textcolor{blue}{0} \\
  0 & 1 & 1 & 1 & 0 & 1 & \textcolor{red}{0} & \textcolor{blue}{0} \\
  \textcolor{red}{1} & \textcolor{red}{1} & \textcolor{red}{0} & \textcolor{red}{1} & \textcolor{red}{0} & \textcolor{red}{0} & \textcolor{red}{1} & \textcolor{blue}{0} \\
  \textcolor{blue}{1} & \textcolor{blue}{1} & \textcolor{blue}{1} & \textcolor{blue}{0} & \textcolor{blue}{0} & \textcolor{blue}{0} & \textcolor{blue}{0} & \textcolor{blue}{1}
\end{pmatrix}
$

Ayant placé l'identité en bloc d'un seul côté, on peut procéder
à une transformation simple de $H_2$ pour obtenir $G_2$.

$
G_2 = 
\begin{pmatrix}
  1 & 0 & 0 & 0 & 1 & 0 & \textcolor{red}{1} & \textcolor{blue}{1} \\
  0 & 1 & 0 & 0 & 0 & 1 & \textcolor{red}{1} & \textcolor{blue}{1} \\
  0 & 0 & 1 & 0 & 1 & 1 & \textcolor{red}{0} & \textcolor{blue}{1} \\
  0 & 0 & 0 & 1 & 1 & 1 & \textcolor{red}{1} & \textcolor{blue}{0}
\end{pmatrix}
$


\subsubsection{Détection et correction d'erreurs}

Étudions le décodage du mot 0000(0000) lorsque celui-ci est reçu sans erreur
ou avec une à deux erreurs dont les positions varient.

\begin{itemize}
  \item{
    Réception sans erreur\,:
    
    $
    H_2 \cdot C^t = 
    \begin{pmatrix}
      1 & 0 & 1 & 1 & 1 & 0 & 0 & 0 \\
      0 & 1 & 1 & 1 & 0 & 1 & 0 & 0 \\
      1 & 1 & 0 & 1 & 0 & 0 & 1 & 0 \\
      1 & 1 & 1 & 0 & 0 & 0 & 0 & 1
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      0 \\
      0 \\
      0 \\
      0 \\
      0 \\
      0 \\
      0 \\
      0
    \end{pmatrix}
    =
    0_{4,1}
    $
    
    Lorsqu'un message est transmis sans erreur, on obtient après décodage
    la matrice $0_{4,1}$.
    \vspace{5mm}
  }
  \item{
    Réception avec une unique erreur\,:

    $
    H_2 \cdot C^t = 
    \begin{pmatrix}
      1 & 0 & 1 & 1 & 1 & 0 & 0 & 0 \\
      0 & 1 & 1 & 1 & 0 & 1 & 0 & 0 \\
      1 & 1 & 0 & 1 & 0 & 0 & 1 & 0 \\
      1 & 1 & 1 & 0 & 0 & 0 & 0 & 1
    \end{pmatrix}
    \cdot
    \begin{pmatrix}
      1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
      0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\
      0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\
      0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\
      0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\
      0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\
      0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \\
      0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 
    \end{pmatrix}
    =
    \begin{pmatrix}
      1 & 0 & 1 & 1 & 1 & 0 & 0 & 0 \\
      0 & 1 & 1 & 1 & 0 & 1 & 0 & 0 \\
      1 & 1 & 0 & 1 & 0 & 0 & 1 & 0 \\
      1 & 1 & 1 & 0 & 0 & 0 & 0 & 1
    \end{pmatrix}
    $

    Lorsqu'un message est transmis avec une unique erreur, une colonne non nulle
    est obtenue après décodage. Celle-ci est identique à une des colonnes
    de $H_2$ dont le numéro correspond à la position de l'erreur. 
    \vspace{5mm}
  }

  \newpage

  \item{
    Réception avec deux erreurs\,:

    \begin{itemize}
      \item{
        Tests avec une des erreurs fixée dans un bit de donnée\,:

        $
        H_2 \cdot C^t = 
        \begin{pmatrix}
          1 & 0 & 1 & 1 & 1 & 0 & 0 & 0 \\
          0 & 1 & 1 & 1 & 0 & 1 & 0 & 0 \\
          1 & 1 & 0 & 1 & 0 & 0 & 1 & 0 \\
          1 & 1 & 1 & 0 & 0 & 0 & 0 & 1
        \end{pmatrix}
        \cdot
        \begin{pmatrix}
          1 & 1 & 1 & 1 & 1 & 1 & 1 \\
          1 & 0 & 0 & 0 & 0 & 0 & 0 \\
          0 & 1 & 0 & 0 & 0 & 0 & 0 \\
          0 & 0 & 1 & 0 & 0 & 0 & 0 \\
          0 & 0 & 0 & 1 & 0 & 0 & 0 \\
          0 & 0 & 0 & 0 & 1 & 0 & 0 \\
          0 & 0 & 0 & 0 & 0 & 1 & 0 \\
          0 & 0 & 0 & 0 & 0 & 0 & 1 
        \end{pmatrix}
        =
        \begin{pmatrix}
          1 & 0 & 0 & 0 & 1 & 1 & 1 \\
          1 & 1 & 1 & 1 & 1 & 0 & 0 \\
          0 & 1 & 0 & 1 & 1 & 0 & 1 \\
          0 & 0 & 1 & 1 & 1 & 1 & 0
        \end{pmatrix}
        $
        \vspace{5mm}
      }
      \item{
        Tests avec une des erreurs fixée dans un bit de contrôle\,:

        $
        H_2 \cdot C^t = 
        \begin{pmatrix}
          1 & 0 & 1 & 1 & 1 & 0 & 0 & 0 \\
          0 & 1 & 1 & 1 & 0 & 1 & 0 & 0 \\
          1 & 1 & 0 & 1 & 0 & 0 & 1 & 0 \\
          1 & 1 & 1 & 0 & 0 & 0 & 0 & 1
        \end{pmatrix}
        \cdot
        \begin{pmatrix}
          1 & 0 & 0 & 0 & 0 & 0 & 0 \\
          0 & 1 & 0 & 0 & 0 & 0 & 0 \\
          0 & 0 & 1 & 0 & 0 & 0 & 0 \\
          0 & 0 & 0 & 1 & 0 & 0 & 0 \\
          1 & 1 & 1 & 1 & 1 & 1 & 1 \\
          0 & 0 & 0 & 0 & 1 & 0 & 0 \\
          0 & 0 & 0 & 0 & 0 & 1 & 0 \\
          0 & 0 & 0 & 0 & 0 & 0 & 1 
        \end{pmatrix}
        =
        \begin{pmatrix}
          0 & 1 & 0 & 0 & 1 & 1 & 1 \\
          0 & 1 & 1 & 1 & 1 & 0 & 0 \\
          1 & 1 & 0 & 1 & 0 & 1 & 0 \\
          1 & 1 & 1 & 0 & 0 & 0 & 1
        \end{pmatrix}
        $
    
      }

    \end{itemize}

    Lorsqu'un message est transmis avec exactement deux erreurs,
    une colonne non nulle est obtenue après décodage.
    Cette colonne n'étant pas présente dans $H_2$, la combinaison
    de deux colonnes de $H_2$ n'étant, par construction, pas présente
    dans $H_2$, on peut différencier ce cas
    d'une erreur simple.
  }
\end{itemize}


\subsubsection{Multiplicité des couples de matrices de parité et génératrice}

Comme expliqué dans la partie \ref{const_H2G2}, pour assurer
la correction simple couplée à une détection double, la matrice de parité doit
posséder les caractéristiques suivantes\,:

\begin{enumerate}
  \item{Elle doit contenir l'identité,}
  \item{
    Aucune de ses colonnes ne doit être nulle ce qui permet de discerner
    un message reçu sans erreur d'un message erroné,
  }
  \item{
    Chacune de ses colonnes doit être unique afin de pouvoir déterminer
    la position d'une erreur,
  }
  \item{
    La combinaison linéaire de deux colonnes ne doit correspondre
    à aucune des colonnes de la matrice, permettant la détection
    d'erreurs doubles.
    Ainsi et comme expliqué dans la partie \ref{const_H2G2},
    chacune des colonnes de la matrice de parité qui n'est pas une colonne
    de l'identité doit alors comprendre exactement trois 1.
    Ce faisant, la sous-matrice ${A_2}^t$ est donc composée
    des $\dbinom{4}{3} = 4$ arrangements possibles de trois 1 sur 4 positions.
  }
\end{enumerate}

La matrice géneratrice est ensuite construite à partir de la matrice de parité.

Le couple $(H_2,G_2)$ présenté dans ce rapport n'est pas le seul qui possède
ces propriétés. C'est aussi le cas par exemple du couple suivant\,:

$
{H_2}' = 
\begin{pmatrix}
  1 & 0 & 1 & 1 & 1 & 0 & 0 & 0 \\
  0 & 1 & 1 & 1 & 0 & 1 & 0 & 0 \\
  1 & 1 & 1 & 0 & 0 & 0 & 1 & 0 \\
  1 & 1 & 0 & 1 & 0 & 0 & 0 & 1
\end{pmatrix}
\hspace{5mm}
{G_2}' = 
\begin{pmatrix}
  1 & 0 & 0 & 0 & 1 & 0 & 1 & 1 \\
  0 & 1 & 0 & 0 & 0 & 1 & 1 & 1 \\
  0 & 0 & 1 & 0 & 1 & 1 & 1 & 0 \\
  0 & 0 & 0 & 1 & 1 & 1 & 0 & 1
\end{pmatrix}
$

De plus, si l'on supprime la contrainte d'imbrication de $(H_0, G_0)$
dans $(H_2, G_2)$, on peut alors écrire
$
H_2 = 
\left(
\begin{array}{c|c}
  {A_2}^t & I_4 \\
\end{array}
\right)
$.
Comme ${A_2}^t$ doit contenir les 4 arrangements possibles des trois 1 dans
ses colonnes, il existe $4! = 24$ permutations des lignes de ${A_2}^t$, soit
autant de couples comme par exemple celui-ci où l'on a permuté
la 3\up{ème} et 4\up{ème} ligne de ${{A_2}'}^t$\,:

$
{H_2}'' = 
\begin{pmatrix}
  1 & 0 & 1 & 1 & 1 & 0 & 0 & 0 \\
  1 & 1 & 1 & 0 & 0 & 1 & 0 & 0 \\
  0 & 1 & 1 & 1 & 0 & 0 & 1 & 0 \\
  1 & 1 & 0 & 1 & 0 & 0 & 0 & 1
\end{pmatrix}
\hspace{5mm}
{G_2}'' = 
\begin{pmatrix}
  1 & 0 & 0 & 0 & 1 & 1 & 0 & 1 \\
  0 & 1 & 0 & 0 & 0 & 1 & 1 & 1 \\
  0 & 0 & 1 & 0 & 1 & 1 & 1 & 0 \\
  0 & 0 & 0 & 1 & 1 & 0 & 1 & 1
\end{pmatrix}
$

Enfin, il est également possible d'effectuer des permutations de colonnes,
mais, dans le cas où l'identité n'est plus en un seul bloc, déterminer
la matrice géneratice demande légèrement plus de travail (cf. \ref{const_H0G0}).


\subsubsection{Bilan}

Les codes de Hamming sont les meilleurs codes 1-correcteurs.
Ainsi, ils possèdent tous une distance de Hamming de 3
(distance minimale pour pouvoir corriger 1 erreur).

Le présent code n'est pas le meilleur pour corriger une unique erreur.
Il permet cependant de le faire tout en distinguant jusqu'à 2 erreurs,
en différenciant ces deux cas. Ayant une distance de Hamming de 4, ce code
permet techniquement de distinguer jusqu'à 3 erreurs, mais ce faisant,
il perd sa capacité correctrice car il ne permet pas de distinguer
3 erreurs d'une erreur unique,
tout comme les codes de Hamming peuvent soit détecter 2 erreurs soit
en corriger une seule
(ce code possédant une distance de Hamming de 4, un mot à distance 3
de son mot d'origine est à distance 1 d'un autre mot du code).

Le couple $(H_2, G_2)$ permet un rendement de $\frac{1}{2}$ contre $\frac{4}{7}$
pour un Hamming(7,4).
Ainsi pour transmettre toutes les données avec $(H_2, G_2)$, il faut
envoyer l'équivalent de 2 fois la quantité de données utiles contre seulement
1.75 fois en utilisant un code de Hamming(7,4).
Le rendement est en effet perdu au profit de la capacité
de correction couplée à une capacité de détection de 2 erreurs simultanées.
Il est donc plus efficace d'utiliser un code de Hamming(7,4) lorsque le taux
d'erreurs doubles par tranche de 7 bits est inexistant (car ces erreurs seraient
alors mal corrigées) ou que le taux d'erreurs simples comme doubles est faible
tout comme le coût d'une retransmission (aucune correction n'est alors
effecuée). Le présent code, quant à lui, est
plus efficace lorsque la probabilité d'une erreur double est non nulle sur
la transmission de paquets de 8 bits et que le coût de retransmission
étant important, il vaut mieux être capable de corriger une erreur simple que de
demander sa retransmission (mais une erreur double pouvant survenir,
il faut être capable de la différencier d'une erreur simple).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implémentation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{Implémentation}

\subsection{Remarques générales}

Afin de pouvoir facilement tester différentes matrices $H$ et $G$, j'ai choisi
de les rendre «~visibles~» dans le code. Ainsi, l'encodage et le décodage
des messages se fait par un parcours de la matrice appropriée. Celle-ci étant
déjà déterminée à la compilation, il aurait été plus performant de directement
écrire le calcul pour chacun des bits de contrôle (et de directement recopier
les bits de données à leur position puisqu'ils restent inchangés). 

De plus, les \texttt{CodeWord} étant composés de 2 mots, leur encodage et leur
décodage se fait en deux étapes similaires (encodage du premier mot puis
du deuxième suivant la même méthode). J'avais initialement prévu de mettre ces
étapes dans une boucle afin de ne pas dédoubler le code, mais je trouvais que
cela compléxifiait inutilement sa compréhension. J'ai donc décidé de supprimer
la boucle et de laisser le code dédoublé.

Afin de simplifier les explications dans la suite du rapport, je vais considérer
que les mots envoyés (\texttt{CodeWord}) ne sont constitués que
d'une seule partie donnée et d'une seule partie contrôle.


\subsection{Émetteur}

Le $i$\up{ème} bit d'un mot encodé peut s'écrire comme somme modulo 2 (ou xor)
du produit (ou bitwise and) des bits de données avec la $i$\up{ème} colonne
de la matrice G.

L'algorithme d'encodage/encapsulation est alors le suivant\,:

\begin{algorithm}[H]
  \Begin{
    \comment{// Pour chacun des mots\\}
    \For{$i \leftarrow 0$ \KwTo $\mathit{nb\_mots}$}{
      \comment{// Pour chacun des bits du mot envoyé\\}
      \For{$j \leftarrow 0$ \KwTo $\mathit{l\_env}$}{
        \vspace{1mm}
        $\mathit{env}[j] \leftarrow
        \overset{\mathit{l\_data}}{\underset{k \leftarrow 0}{\bigoplus}}
        \mathit{data_i}[k] \hspace{1mm} \& \hspace{1mm} G_{k,j}$
      }
    }
  }
  \caption{Encapsulation des données et contrôle}
\end{algorithm}


\newpage
\subsection{Récepteur}

La réception d'un mot commence par la correction des erreurs simples. \\
On repère une erreur si $H \cdot C^t \neq 0_4$. Elle est simple
(par conséquent corrigeable) si ce produit est égale à l'une des colonnes de
$H$ (le numéro de la colonne indiquant la position de l'erreur).

\begin{algorithm}[H]
  \Begin{
    \comment{// Pour chacun des mots reçus\\}
    \For{$i \leftarrow 0$ \KwTo $\mathit{nb\_mots}$}{
      \comment{// Pour chacune des lignes de H\\}
      \For{$j \leftarrow 0$ \KwTo $\mathit{l\_data}$}{
        \vspace{1mm}
        $\mathit{err}[j] \leftarrow
        \overset{\mathit{l\_rec}}{\underset{k \leftarrow 0}{\bigoplus}}
        H_{j,k} \hspace{1mm} \& \hspace{1mm} \mathit{rec_i}[k]$
        }
        
      \vspace{3mm}
      \comment{// Pour chaque colonne de H \\}
      \For{$j \leftarrow 0$ \KwTo $\mathit{l\_rec}$}{
        \comment{// Vrai pour au plus 1 colonne \\}
        \If{err = $H_j$}{
          
          inverser($\mathit{rec_i}[j]$)
        }
      }
    }
  }
  \caption{Détection et correction des erreurs simples}
\end{algorithm}


Il faut ensuite vérifier la présence d'erreur double.
Pour simplifier la correction d'erreurs simples,
j'ai remplacé les bits de contrôle par le résultat de $H \cdot C^t$.
Ainsi, si ces bits sont non nuls mais ne correspondent à aucune colonne de H,
on peut en déduire qu'une erreur double est survenue.

Pour améliorer cette détection, j'ai remplacé les bits de contrôle par 0
lorsqu'une correction d'erreur simple était effectuée.
Ainsi, plus besoin de comparer chaque colonne de H.
En effet, si les bits de contrôle sont non nuls,
ils ne correspondent à aucune colonne de H car dans le cas contraire,
une correction d'erreur simple aurait eu lieu remettant ces bits à 0.
Il suffit donc simplement de vérifier qu'ils soient bien nuls.

L'algorithme est donc le suivant\,:

\begin{algorithm}[H]
  \Begin{
    \comment{// Pour chacun des mots\\}
    \For{$i \leftarrow 0$ \KwTo $\mathit{nb\_mots}$}{
      \vspace{1mm}
      $err \leftarrow
      \overset{\mathit{l\_contr}}{\underset{j \leftarrow 0}{\sum}}
      controle_i[j]
      $
      
      \vspace{1mm}
      \If{$\mathit{err} \neq 0$}{
        traiter\_double\_erreur()
      }
    }
  }
  \caption{Vérification des erreurs doubles}
\end{algorithm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conclusion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{Conclusion}

Ce projet a permis la mise en place d'un code permettant la correction simple
couplée à une détection double. Pour ce faire, il a fallu modifier
des erreurs de conception de la matrice de parité initiale donnée,
ainsi qu'augmenter la distance de Hamming la faisant passer de 2 à 4.

J'ai apprécié le fait que ce projet soit axé sur la conception
et que la place laissée à l'implémentation y soit réduite (notamment grâce
aux parties déjà codées). En outre, j'ai trouvé la réflexion demandée sur la construction
et l'unicité de la matrice $H_2$ particulièrement enrichissante.

\end{document}